package com.example.halis.cookinggallery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author Halis
 * @date 21.09.2015.
 * DBHandler takes care of SQLite related functions
 * Sets up database, write info into it and show the database
 */
public class DBHandler extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 10;
    private static final String DATABASE_NAME = "recipes.db";
    private static final String TABLE_RECIPES = "recipes";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "recipeTitle";
    public static final String COLUMN_IMG = "recipeIMG";
    public static final String COLUMN_INGREDIENTS = "recipeIngredients";
    public static final String COLUMN_PROGRESS = "recipeProgress";


    public DBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE " + TABLE_RECIPES + "(" +
               COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
               COLUMN_TITLE + " TEXT, " +
               COLUMN_IMG + " TEXT, " +
               COLUMN_INGREDIENTS + " TEXT, " +
               COLUMN_PROGRESS + " TEXT " +
                ");";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPES);
        onCreate(db);

    }

    public void addRecipe(String recipeTitle, String recipeIMG, String recipeIngredients, String recipeProgress){
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, recipeTitle);
        values.put(COLUMN_IMG, recipeIMG);
        values.put(COLUMN_INGREDIENTS, recipeIngredients);
        values.put(COLUMN_PROGRESS, recipeProgress);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_RECIPES, null, values);
        db.close();

    }

    public String dbToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_RECIPES + " WHERE 1;";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        while(!c.isAfterLast()){
            if (c.getString(c.getColumnIndex("recipeTitle")) != null) {
                dbString += c.getString(c.getColumnIndex("recipeTitle"));
                dbString += "\n";
                dbString += " " + c.getString(c.getColumnIndex("recipeIMG"));
                dbString += "\n";
                dbString += " " + c.getString(c.getColumnIndex("recipeIngredients"));
                dbString += "\n";
                dbString += " " + c.getString(c.getColumnIndex("recipeProgress"));
                dbString += "\n\n";
            }
            c.moveToNext();
        }
        db.close();
        c.close();
        return dbString;
    }


}

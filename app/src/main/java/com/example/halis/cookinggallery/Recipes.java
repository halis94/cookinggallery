package com.example.halis.cookinggallery;

/**
 * @author Halis
 * @date 21.09.2015
 */
public class Recipes {

    private int _id;
    private String _title;
    private String _image;
    private String _ingredients;
    private String _progress;

    public Recipes(){

    }

    public Recipes(String recipeTitle) {this._title = recipeTitle;}

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public void set_image(String _image) {
        this._image = _image;
    }

    public void set_ingredienser(String _ingredients) {
        this._ingredients = _ingredients;
    }

    public void set_fremgang(String _progress) {
        this._progress = _progress;
    }

    public int get_id() {
        return _id;
    }

    public String get_title() {
        return _title;
    }

    public String get_image() {
        return _image;
    }

    public String get_ingredients() {
        return _ingredients;
    }

    public String get_progress() {
        return _progress;
    }


}

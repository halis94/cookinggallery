package com.example.halis.cookinggallery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

/**
 * @author Halis
 * @date 21.09.2015
 * Activity where user can see recipes which has been added to database.
 */
public class MainActivity extends AppCompatActivity {
    DBHandler dbHandler;

    public void goToSQL(View view)
    {
        Intent intent = new Intent(MainActivity.this, SQLActivity.class);
        startActivity(intent);
    }

    public void goToOptions(View view)
    {
        Intent intent = new Intent(MainActivity.this, OptionsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView recipesView = (TextView) findViewById(R.id.recipesView);
        dbHandler = new DBHandler(this, null, null, 5);
        recipesView.setText(dbHandler.dbToString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}

package com.example.halis.cookinggallery;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * @author Halis
 * @date 21.09.2015
 * Define values which will be send to database from EditTexts.
 * Take care of how camera will open.
 * Send values by button click to database which come from EditTexts
 */
public class SQLActivity extends AppCompatActivity {

    public void goToMain(View view) {
        Intent intent = new Intent(SQLActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void goToOptions(View view) {
        Intent intent = new Intent(SQLActivity.this, OptionsActivity.class);
        startActivity(intent);
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;
    EditText recipeTitleText;
    EditText recipeImgText;
    EditText recipeIngredientsText;
    EditText recipeProgressText;
    DBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sql);

        Button cameraButton = (Button) findViewById(R.id.cameraButton);


        //disable button if the user doesn't have a camera
        if (!hasCamera())
            cameraButton.setEnabled(false);

        recipeTitleText = (EditText) findViewById(R.id.dbTitleText);
        recipeImgText = (EditText) findViewById(R.id.dbImgText);
        recipeIngredientsText = (EditText) findViewById(R.id.dbIngredientsText);
        recipeProgressText = (EditText) findViewById(R.id.dbDirectionsText);
        dbHandler = new DBHandler(this, null, null, 10);

        resetFields();

    }

    private boolean hasCamera() {

        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }

    public void launchCamera(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // TAKE a picture and pass results along to onActivityResult
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    public void resetFields() {
        recipeTitleText.setText("");
        recipeImgText.setText("");
        recipeIngredientsText.setText("");
        recipeProgressText.setText("");
    }

    public void addButtonClick(View view) {
        String recipeTitle = recipeTitleText.getText().toString();
        String recipeIMG = recipeImgText.getText().toString();
        String recipeIngredients = recipeIngredientsText.getText().toString();
        String recipeProgress = recipeProgressText.getText().toString();
        dbHandler.addRecipe(recipeTitle, recipeIMG, recipeIngredients, recipeProgress);
        ImageView photoView = (ImageView)findViewById(R.id.photoView);
        photoView.setImageBitmap(BitmapFactory.decodeFile(recipeIMG));
        resetFields();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sql, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            /* get the photo */
            Uri photoMap = data.getData();


            String[] filePathColumn = {MediaStore.Images.Media.DATA};


            Cursor cursor = getContentResolver().query(photoMap, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            recipeImgText.setText(picturePath);

        }

    }

}
